const bodyParser = require("body-parser");
//const express = require("body-parser");
const express = require("express");
const router = express.Router();



let datos = [{
    matricula:"2019030399   ",
    nombre:"ACOSTA ORTEGA JESUS HUMBERTO    ",
    sexo:'M ',
    materias:["Ingles", "Base de datos", "Tecnologia I  "]
},
{
    matricula:" 2020030310   ",
    nombre:" ACOSTA VARELA IRVING GUADALUPE",
    sexo:' M ',
    materias:[" Ingles", " Base de datos", " Tecnologia I  "]
},
{
    matricula:" 202003007    ",
    nombre:" ALMOGABAR VAZQUES YARLEN DE JESUS   ",
    sexo:' F ',
    materias:["Ingles", " Base de datos", " Tecnologia I  "]
}

]
router.get("/",(req,res)=>{

    res.render('index.html',{titulo: "Pagina en Embedded JavaScript EJS",listado:datos ,nombre: "Cerezo Arreola José Manuel",materia: "TAI", grupo: "8-3"});
    
});

router.get("/tabla", (req, res)=> {
    const params = {
        numero: req.query.numero
    }
    res.render("tabla.html", params);
});

router.post("/tabla", (req, res)=> {
    const params = {
        numero: req.body.valor.pinicial.plazo
    }
    res.render("tabla.html", params);
});
router.get("/cotizacion",(req,res)=>{
    const params ={
        valor:req.body.valor,
        pinicial:req.body.pinicial,
        plazo:req.body.plazo
    }
    res.render("cotizacion.html", params);
});
router.post("/cotizacion",(req, res)=> {
    const params = {
        valor:req.body.valor,
        pinicial:req.body.pinicial,
        plazo:req.body.plazo
    }
    res.render("cotizacion.html", params);
});
module.exports=router;